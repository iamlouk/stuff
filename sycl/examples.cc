#include <CL/sycl.hpp>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <cassert>

constexpr cl::sycl::access::mode sycl_read       = cl::sycl::access::mode::read;
constexpr cl::sycl::access::mode sycl_write      = cl::sycl::access::mode::write;
constexpr cl::sycl::access::mode sycl_read_write = cl::sycl::access::mode::read_write;

class GPUReduce;
class GPUGauss;
class GPUBlockedGauss;
class GPUMatMulVec;

/*
 * Matrix-Matrix Multiplikation auf der GPU:
 * Rueckgabewert: A * B (A: NxM, B: MxL, res: NxL)
 */
std::vector<double> gpu_matmul(cl::sycl::queue &queue, std::vector<double> A, std::vector<double> B, size_t N, size_t M, size_t L) {
	std::vector<double> C(N * L);

	// 2D-sycl::buffer anlegen:
	cl::sycl::buffer<double, 2> bufA(A.data(), cl::sycl::range<2>(N, M));
	cl::sycl::buffer<double, 2> bufB(B.data(), cl::sycl::range<2>(M, L));
	cl::sycl::buffer<double, 2> bufC((cl::sycl::range<2>(N, L)));

	// Wenn bufC out of scope geht Ergebnisse in C speichern:
	// D.h. auch das diese Funktion erst zurueckkehrt wenn
	// die Berechnung fertig ist, da der Destruktor von bufC blockiert.
	bufC.set_final_data(C.data());

	// In dem hier uebergebenen Lambda die Akzessoren anlegen:
	// Hier laeuft noch alles auf der CPU
	queue.submit([&bufA, &bufB, &bufC, N, M, L](cl::sycl::handler &cgh) -> void {
		// Aus bufA und bufB wird eh nur gelesen, in bufC nur geschrieben:
		// SYCL kopiert/allokiert ggf. jetzt erst Speicher.
		auto Aaccs = bufA.get_access<sycl_read>(cgh);
		auto Baccs = bufB.get_access<sycl_read>(cgh);
		auto Caccs = bufC.get_access<sycl_write>(cgh);

		// Raeumliche Aufteilung der GPU-Threads:
		// Wie gross eine "Work Group" (bzw. in Cuda-Sprech ein Block) ist entscheidet die Laufzeitumgebung.
		auto grid = cl::sycl::range<2>(N, L);

		// Dieses Lambda laeuft dann wirklich parallel auf der GPU:
		// Da die Threadaufteilung 2-dimensional ist ist auch die Pos. im Gitter 2-dimensional:
		cgh.parallel_for<GPUMatMulVec>(grid, [Aaccs, Baccs, Caccs, N, M, L](cl::sycl::item<2> it) -> void {
			// Position dieses Threads:
			size_t i = it.get_id(0), j = it.get_id(1);

			// normale Matrix-Matrix-Multiplikation:
			double sum = 0;
			for (size_t k = 0; k < M; k++)
				sum += Aaccs[i][k] * Baccs[k][j];

			Caccs[i][j] = sum;
		});
	});

	return C;
}

/*
 * Bild unscharf machen:
 */
std::vector<double> gpu_gauss(cl::sycl::queue &queue, int iters, std::vector<double> img, size_t N, size_t M) {
	std::vector<double> res(N * M);

	cl::sycl::buffer<double, 2> buf1(img.data(), cl::sycl::range<2>(N, M));
	cl::sycl::buffer<double, 2> buf2(img.data(), (cl::sycl::range<2>(N, M)));
	buf2.set_final_data(res.data());

	auto grid = cl::sycl::range<2>(N, M);

	auto lambda = [&buf1, &buf2, N, M, grid](cl::sycl::handler &cgh) -> void {
		auto accs1 = buf1.get_access<sycl_read>(cgh);
		auto accs2 = buf2.get_access<sycl_write>(cgh);

		cgh.parallel_for<GPUGauss>(grid, [accs1, accs2, N, M](cl::sycl::item<2> it) -> void {
			size_t i = it.get_id(0), j = it.get_id(1);

			if (i == 0 || i == N - 1 || j == 0 || j == M - 1)
				return;

			accs2[i][j] = (
				accs1[i - 1][j] +
				accs1[i][j - 1] +
				accs1[i][j + 1] +
				accs1[i + 1][j]) * 0.25;
		});
	};

	for (int i = 0; i < iters; i++) {
		queue.submit(lambda);
		std::swap(buf1, buf2);
	}

	return res;
}

/*
 * Cache-Blocked Bild unscharf machen:
 * Funktioniert nur bei N/M die ein Vielfaches der blocksize sind.
 */
std::vector<double> gpu_blockedgauss(cl::sycl::queue &queue, int iters, std::vector<double> mat, size_t N, size_t M) {
	std::vector<double> res(N * M);

	cl::sycl::buffer<double, 2> buf1(mat.data(), cl::sycl::range<2>(N, M));
	cl::sycl::buffer<double, 2> buf2(mat.data(), (cl::sycl::range<2>(N, M)));
	buf2.set_final_data(res.data());

	const size_t blocksizeN = 8, blocksizeM = 8;

	// nd_range verwenden um selbst die Work-Group-Groesse entscheiden zu koennen.
	auto grid = cl::sycl::nd_range<2>{
		cl::sycl::range<2>{ N, M }, // Gesammtgitter-Groesse
		cl::sycl::range<2>{ blocksizeN, blocksizeM } // Block-Groesse
	};


	auto lambda = [&buf1, &buf2, N, M, blocksizeN, blocksizeM, grid](cl::sycl::handler &cgh) -> void {
		// Akzessoren: Aus dem einen wird gelesen, in den anderen geschrieben,
		// und zwischen zwei Kernel-Starts werden die buffer geswapt.
		auto accs1 = buf1.get_access<sycl_read>(cgh);
		auto accs2 = buf2.get_access<sycl_write>(cgh);

		// Von Bloecken/Work-Groups geteilter Speicher (nur in je einem Kernel-Start verfuegbar).
		// Weil die Randpunkte auch gebraucht werden muss dieser Speicher groesser sein als nur blocksizeN * blocksizeM.
		auto cache = cl::sycl::accessor<double, 2, sycl_read_write, cl::sycl::access::target::local>(cl::sycl::range<2>(blocksizeN + 2, blocksizeM + 2), cgh);

		cgh.parallel_for<GPUBlockedGauss>(grid, [accs1, accs2, cache, N, M, blocksizeN, blocksizeM](cl::sycl::nd_item<2> it) -> void {
			// gi, gj: Gitterweite Position (Cuda: blockIdx.x * blockDim.x + threadIdx.x)
			size_t gi = it.get_global_id(0), gj = it.get_global_id(1);
			// li, lj: Lokale bzw. Blockbezogene Position (threadIdx.x)
			size_t li = it.get_local_id(0),  lj = it.get_local_id(1);

			// den expliziten cache fuellen (innere Werte):
			cache[li + 1][lj + 1] = accs1[gi][gj];

			// den expliziten cache fuellen (Randwerte):
			if (li == 0 && gi > 0)
				cache[li + 0][lj + 1] = accs1[gi - 1][gj + 0];
			if (li == (blocksizeN - 1) && gi < N - 1)
				cache[li + 2][lj + 1] = accs1[gi + 1][gj + 0];
			if (lj == 0 && gj > 0)
				cache[li + 1][lj + 0] = accs1[gi + 0][gj - 1];
			if (lj == (blocksizeM - 1) && gj < M - 1)
				cache[li + 1][lj + 2] = accs1[gi + 0][gj + 1];

			// Alle Threads in einem Block/Work-Group synchronisieren.
			// Bisher wurde geschrieben, ab jetzt nur noch gelesen.
			it.barrier(cl::sycl::access::fence_space::local_space);

			// Bildraender koennen nicht verschwommen werden:
			if (gi == 0 || gi == N - 1 || gj == 0 || gj == M - 1)
				return;

			// Neuen Pixel berechnen und dabei aber auf geteilten Speicher zugreifen:
			accs2[gi][gj] = (
				cache[li + 0][lj + 1] +
				cache[li + 1][lj + 0] +
				cache[li + 1][lj + 2] +
				cache[li + 2][lj + 1]) * 0.25;
		});
	};

	for (int i = 0; i < iters; i++) {
		// `parallel_for` kann man nur 1x pro "command group" ausfuehren,
		// deswegen einfach selbe "command group" iters oft submitten.
		// `parallel_for`-Lambda laeuft asynchron, aber das submit uebergebene lambda selbst nicht.
		queue.submit(lambda);
		std::swap(buf1, buf2);
	}

	return res;
}

/*
 * Summiert alle Werte in `vals` auf und gibt die Summe zurueck.
 * `vals` bleibt unveraendert. Funktioniert nur fuer Zweierpotenzen!
 */
int gpu_reduce(cl::sycl::queue &queue, std::vector<int> vals) {
	size_t blocksize = 32;
	size_t n = vals.size();

	// Buffer fuer den aufzusummierenden vector:
	cl::sycl::buffer<int, 1> buf(vals.data(), cl::sycl::range<1>(n));

	// Da wir nicht wollen das temp. Summen zurueckgeschrieben werden:
	buf.set_final_data(nullptr);

	for (int i = 1; n > 1; i++) {
		queue.submit([&buf, blocksize, &n](cl::sycl::handler &cgh) -> void {
			auto buffaccs = buf.get_access<sycl_read_write>(cgh);

			// Lokaler Specher (Unter Cuda "shared memory"):
			// Wird zwischen Kernelstarts quasi zurueckgesetzt, dafuer sehr viel schneller als globaler Speicher:
			auto local = cl::sycl::accessor<
				int, 1, sycl_read_write, 
				cl::sycl::access::target::local>(cl::sycl::range<1>(blocksize), cgh);

			// `nd_range` weil wir hier selbst entscheiden wollen wie gross eine "Work Group" ist,
			// und nicht SYCL fuer uns entscheiden lassen wollen.
			auto grid = cl::sycl::nd_range<1>{
				cl::sycl::range<1>{ std::max(n, blocksize) },
				cl::sycl::range<1>{ std::min(blocksize, n) }
			};

			cgh.parallel_for<GPUReduce>(grid, [blocksize, buffaccs, local, n](cl::sycl::nd_item<1> id) -> void {
				// Weil der Kernel mit einer `nd_range` gestartet wurde hat man hier jetzt auch ein `nd_item`.
				// `gridid` ist die Position relativ zum Gesammtgitter, `localid` relativ zur eigenen Gruppe.
				// Eine Gruppe kann gemeinsam den schnellen lokalen Speicher nutzen.
				size_t gridid = id.get_global_id(0);
				size_t localid = id.get_local_id(0);

				if (gridid < n)
					local[localid] = buffaccs[gridid];

				// Gruppen koennen sich ausserdem miteinander synchronisieren:
				// Cuda-Aequivalent: `__syncthreads()`
				id.barrier(cl::sycl::access::fence_space::local_space);

				// normale parallele Reduktion im geteilten Speicher:
				for (int stride = 1; stride < blocksize; stride *= 2) {
					if (localid % (2 * stride) == 0 && gridid < n && localid + stride < n)
						local[localid] += local[localid + stride];

					// Zwischen Lesen und Schreiben immer synchronisieren!
					// Synchronisierungsoverhead in einer "Work Group" klein.
					id.barrier(cl::sycl::access::fence_space::local_space);
				}

				// Ergebniss zurueckschreiben:
				if (localid == 0 && gridid < n)
					buffaccs[id.get_group(0)] = local[0];

			});
		});

		// In einer Iter. werden parallel blocksize viele Werte auf einen Zusammengefasst:
		n /= blocksize;
	}

	// Host-Akzessor um einen einzelnen Wert von dem Host aus auslesen zu koennen:
	// Hier wird SYCL automagisch blockieren/synchronisieren.
	auto buffaccs = buf.get_access<sycl_read>();
	return buffaccs[0];
}

void print_matrix(std::vector<double> A, size_t N, size_t M) {
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++)
			printf("%2.2f\t", A[(i * M) + j]);
		printf("\n");
	}
	printf("\n");
}

int main(int argc, char* argv[]) {
	
	// `parallel_for` und Speichertransfer-Aufrufe sind u.a. asynchron,
	// sollte ein Fehler asynchron auftreten kommt in diesem Lambda der
	// Fehler an:
	cl::sycl::queue queue([](cl::sycl::exception_list exceptions) -> void {
		try {
			for (auto &exception : exceptions)
				std::rethrow_exception(exception);
		} catch (cl::sycl::exception ex) {
			fprintf(stderr, "SYCL Exception: %s\n", ex.what());
			exit(EXIT_FAILURE);
		}
	});

	// Quasi wie bei OpenCL:
	printf("Device: %s, Platform: %s\n",
		queue.get_device().get_info<cl::sycl::info::device::name>().c_str(),
		queue.get_device().get_platform().get_info<cl::sycl::info::platform::name>().c_str());

	std::vector<int> vals(32 * 4);
	for (int i = 0; i < vals.size(); i++)
		vals[i] = i + 1;

	int sum = gpu_reduce(queue, vals);

	printf("#1.: sum(1 to %lu): %d (expected: %lu)\n",
		vals.size() + 1, sum,
		((vals.size() + 1) * vals.size()) / 2);

	std::vector<double> A = {
		1., 2., 3.,
		3., 4., 5.,
		5., 6., 7.
	};
	std::vector<double> B = {
		1., 0., 0.,
		0., 1., 0.,
		0., 0., 1.
	};
	std::vector<double> C = gpu_matmul(queue, A, B, 3, 3, 3);
	printf("#2.: matrix multiplication:\n");
	print_matrix(C, 3, 3);



	std::vector<double> img = {
		1., 1., 1., 1., 1., 1.,
		1., 0., 0., 0., 0., 0.,
		1., 0., 0., 0., 0., 0.,
		1., 0., 0., 0., 0., 0.,
		1., 0., 0., 0., 0., 0.,
		1., 0., 0., 0., 0., 0.,
	};

	std::vector<double> blured = gpu_gauss(queue, 10, img, 6, 6);
	printf("#3.: blur image:\n");
	print_matrix(blured, 6, 6);


	size_t N = 128, M = 128;
	std::vector<double> img2(N * M);
	for (int i = 0; i < N * M; i++)
		img2[i] = i * 2;

	auto blured1 = gpu_gauss(queue, 10, img2, N, M);
	auto blured2 = gpu_blockedgauss(queue, 10, img2, N, M);

	//print_matrix(blured1, N, M);

	//print_matrix(blured2, N, M);

	for (int i = 0; i < N * M; i++)
		assert(blured1[i] == blured2[i]);

	printf("#4.: blocked gauss filter: check!\n");

	return EXIT_SUCCESS;
}


