//
//  Rocket.swift
//  Space Game
//
//  Created by Lou Knauer on 24.10.15.
//  Copyright © 2015 LouK. All rights reserved.
//

import Foundation
import SpriteKit

class Rocket: SKSpriteNode {
    let direction:Direction
    
    init(direction: Direction, pos: CGPoint) {
        let texture = SKTexture(imageNamed: "Rakete")
        let angle = toRadiant( Double(direction.rawValue) )
        
        self.direction = direction
        
        super.init(texture: texture, color: NSColor.clearColor(), size: texture.size())
        
        self.position.x = pos.x
        self.position.y = pos.y
        self.setScale(0.1)
        self.zPosition = -1;
        self.runAction(SKAction.rotateToAngle(angle, duration: 0))
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: self.size.height / 2)
        self.physicsBody!.affectedByGravity = false
        self.physicsBody!.categoryBitMask = ColliderTypes.rocket.rawValue
        self.physicsBody!.contactTestBitMask = ColliderTypes.enemy.rawValue
        self.physicsBody!.collisionBitMask = ColliderTypes.enemy.rawValue
    }
    
    
    func fire( callback:((Rocket) -> Void) ){
        var point = CGPoint(x: self.position.x, y: self.position.y)
        if direction == Direction.UP {
            self.position.y += 50
            point.y = self.parent!.frame.height / 2 + 20
        } else if (direction == Direction.DOWN) {
            self.position.y -= 50
            point.y = -(self.parent!.frame.height / 2 + 20)
        } else if (direction == Direction.RIGHT) {
            self.position.x += 50
            point.x = self.parent!.frame.width / 2 + 20
        } else if (direction == Direction.LEFT) {
            self.position.x -= 50
            point.x = -(self.parent!.frame.width / 2 + 20)
        } else
        
        if direction == Direction.UP_LEFT {
            point.y = self.parent!.frame.height / 2
            point.x = getXPos(point.y)
            self.position.y += 50
            self.position.x -= 50
        } else if (direction == Direction.UP_RIGHT) {
            point.y = self.parent!.frame.height / 2
            point.x = getXPos(point.y)
            self.position.y += 50
            self.position.x += 50
        } else if (direction == Direction.DOWN_LEFT) {
            point.y = -(self.parent!.frame.height / 2)
            point.x = getXPos(point.y)
            self.position.y -= 50
            self.position.x -= 50
        } else if (direction == Direction.DOWN_RIGHT) {
            point.y = -(self.parent!.frame.height / 2)
            point.x = getXPos(point.y)
            self.position.y -= 50
            self.position.x += 50
        }

        let deltaX = self.position.x - point.x
        let deltaY = self.position.y - point.y
        
        let fly = SKAction.moveTo(point, duration: calcFlyDuration(deltaX: deltaX, deltaY: deltaY))
        self.runAction(SKAction.sequence([
            fly,
            SKAction.runBlock({
                self.removeFromParent()
                callback(self)
            })
        ]))
    }
    
    
    func getXPos(y: CGFloat) -> CGFloat {
        let m = -CGFloat( tan( Double(direction.rawValue) * ( M_PI/180 ) ) )
        
        let t = -(m * self.position.x - self.position.y)
        
        let x = (y - t) / m
        
        return x
    }
    
    
    func calcFlyDuration(deltaX x:CGFloat, deltaY y:CGFloat) -> NSTimeInterval {
        let value = (abs(x) + abs(y)) * 0.0015
        return NSTimeInterval(value)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
