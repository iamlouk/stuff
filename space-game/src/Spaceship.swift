//
//  Spaceship.swift
//  Space Game
//
//  Created by Lou Knauer on 18.10.15.
//  Copyright © 2015 LouK. All rights reserved.
//

import Foundation
import SpriteKit


class Spaceship: SKSpriteNode {
    
    init () {
        let texture = SKTexture(imageNamed: "Spaceship")
        super.init(texture: texture, color: NSColor.clearColor(), size: texture.size())
        
        self.setScale(0.2)
        self.zPosition = 1
        self.runAction(SKAction.rotateToAngle(toRadiant(270), duration: 0))
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: self.size.height / 2)
        self.physicsBody!.affectedByGravity = false
        self.physicsBody!.categoryBitMask = ColliderTypes.spaceship.rawValue
        self.physicsBody!.contactTestBitMask = ColliderTypes.enemy.rawValue
        self.physicsBody!.collisionBitMask = ColliderTypes.enemy.rawValue
    }
    
    let speeds:(x: CGFloat, y: CGFloat) = (3, 3)
    let rotateDuration:NSTimeInterval = 0.35
    var direction = Direction.RIGHT
    
    
    
    func moveShipUp(){
        self.position.y += speeds.y;
        if direction != Direction.UP {
            
            rotateToDirection(Direction.UP)
            direction = Direction.UP
        }
    }
    func moveShipDown(){
        self.position.y -= speeds.y;
        if direction != Direction.DOWN {
            
            rotateToDirection(Direction.DOWN)
            direction = Direction.DOWN
        }
    }
    func moveShipRight(){
        self.position.x += speeds.x;
        if direction != Direction.RIGHT {
            
            rotateToDirection(Direction.RIGHT)
            direction = Direction.RIGHT
        }
    }
    func moveShipLeft(){
        self.position.x -= speeds.x;
        if direction != Direction.LEFT {
            
            rotateToDirection(Direction.LEFT)
            direction = Direction.LEFT
        }
    }
    
    
    func rotateToDirection(direction: Direction){
//        let ist = CGFloat(360 / (2*M_PI)) * self.zRotation //self.direction.rawValue
//        let zu = direction.rawValue
//        
//        
//        var angle:Double = 0
//        
//        let weg1 = zu - ist
//        let weg2 = 360 - (zu-ist)
//        
//        if abs(weg1) > abs(weg2) {
//            angle = Double(weg2)
//        } else {
//            angle = Double(weg1)
//        }
//        
//        print(angle)
        
        
//        let action = SKAction.rotateToAngle(toRadiant( angle ), duration: rotateDuration)
//        let action = SKAction.rotateByAngle(toRadiant( angle ), duration: rotateDuration)
//        let action = SKAction.rotateToAngle(toRadiant( Double(self.direction.rawValue) + angle ), duration: rotateDuration)
        let action = SKAction.rotateToAngle(toRadiant( Double(direction.rawValue) ), duration: 0)
        self.removeActionForKey("rotation")
        self.runAction(action, withKey: "rotation")
        
        
    }
    
    
    
    func moveShipUpLeft(){
        self.position.y += speeds.y;
        self.position.x -= speeds.x;
        if direction != Direction.UP_LEFT {
            
            rotateToDirection(Direction.UP_LEFT)
            direction = Direction.UP_LEFT
        }
    }
    func moveShipUpRight(){
        self.position.y += speeds.y;
        self.position.x += speeds.x;
        if direction != Direction.UP_RIGHT {
            
            rotateToDirection(Direction.UP_RIGHT)
            direction = Direction.UP_RIGHT
        }
    }
    func moveShipDownLeft(){
        self.position.y -= speeds.y;
        self.position.x -= speeds.x;
        if direction != Direction.DOWN_LEFT {
            
            rotateToDirection(Direction.DOWN_LEFT)
            direction = Direction.DOWN_LEFT
        }
    }
    func moveShipDownRight(){
        self.position.y -= speeds.y;
        self.position.x += speeds.x;
        if direction != Direction.DOWN_RIGHT {
            
            rotateToDirection(Direction.DOWN_RIGHT)
            direction = Direction.DOWN_RIGHT
        }
    }
    
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}