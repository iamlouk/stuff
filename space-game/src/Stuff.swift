//
//  Stuff.swift
//  Space Game
//
//  Created by Lou Knauer on 24.10.15.
//  Copyright © 2015 LouK. All rights reserved.
//

import Foundation
import SpriteKit

enum Direction:CGFloat {
    case UP = 0
    case DOWN = 180
    case LEFT = 90
    case RIGHT = 270
    case UP_LEFT = 45
    case UP_RIGHT = 315
    case DOWN_LEFT = 135
    case DOWN_RIGHT = 225
}


enum ColliderTypes:UInt32 {
    case spaceship = 0
    case rocket = 1
    case enemy = 2
    case other = 4
}


func toRadiant(a: Double) -> CGFloat {
    return CGFloat( (2.0 * M_PI)/360 * a )
}


class EventObject {
    var events:[String: [Any]] = [String: [Any]]()
    
    func on(event: String, fn: ([Any?]) -> Void) -> EventObject {
        if let _ = events[event] {
            events[event]!.append(fn)
        } else {
            events[event] = []
            events[event]!.append(fn)
        }
        return self
    }
    
    func emit(event: String, arguments: Any?...){
        if let listener = events[event] {
            for (var i = 0; i<listener.count; i++) {
                if let fn = listener[i] as? (([Any?]) -> Void) {
                    fn(arguments)
                }
            }
        }
    }
    
    func emit(event: String){
        if let listener = events[event] {
            for (var i = 0; i<listener.count; i++) {
                if let fn = listener[i] as? (([Any?]) -> Void) {
                    fn([])
                }
            }
        }
    }
}


class Meto {
    internal static func random(min: CGFloat, max: CGFloat) -> CGFloat {
        return min + Meto.random() * (max-min)
    }
    
    internal static func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / Float(UINT32_MAX))
    }
    
    internal static func toRadiant(a: Double) -> CGFloat {
        return CGFloat( (2.0 * M_PI)/360 * a )
    }

    internal static func randomBool() -> Bool {
        return Meto.random() > 0.5
    }

}

