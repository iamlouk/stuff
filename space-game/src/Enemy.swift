//
//  Enemy.swift
//  Space Game
//
//  Created by Lou Knauer on 25.10.15.
//  Copyright © 2015 LouK. All rights reserved.
//

import Foundation
import SpriteKit

class Enemy: SKSpriteNode {
    var target:CGPoint = CGPoint(x: 0, y: 0)
    
    init() {
        let texture = SKTexture(imageNamed: "Komet")
        super.init(texture: texture, color: NSColor.clearColor(), size: texture.size())
        
        self.setScale(Meto.random(0.5, max: 0.7))
        
        let rotate = SKAction.rotateByAngle(toRadiant(360), duration: NSTimeInterval( Meto.random(2, max: 4) ))
        self.runAction( SKAction.repeatActionForever(rotate) )
        
        self.zPosition = 0
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: self.size.height / 2)
        self.physicsBody!.affectedByGravity = false
        self.physicsBody!.categoryBitMask = ColliderTypes.enemy.rawValue
        self.physicsBody!.contactTestBitMask = ColliderTypes.rocket.rawValue
        self.physicsBody!.collisionBitMask = ColliderTypes.rocket.rawValue
    }
    
    
    func fire(frame: CGRect, callback: (Enemy) -> Void) {
        self.position = getRandomPositionOnFrame(frame)
        
        self.target = getRandomPositionOnFrame(frame)
        
        let duration = NSTimeInterval( Meto.random(2, max: 5) )
        
        let action = SKAction.moveTo(self.target, duration: duration)
        self.runAction(SKAction.sequence([
            action,
            SKAction.runBlock({
                //...
                self.removeFromParent()
                callback(self)
            })
        ]))
    }
    
    
    func getRandomPositionOnFrame(frame: CGRect) -> CGPoint {
        var pos = CGPoint(x: 0, y: 0)
        
        if Meto.randomBool() {
            pos.y = frame.height / 2 * Meto.random(-1, max: 1)
            if Meto.randomBool() {
                pos.x = frame.width / 2 + 100
            } else {
                pos.x = -(frame.width / 2 + 100)
            }
        } else {
            pos.x = frame.width / 2 * Meto.random(-1, max: 1)
            if Meto.randomBool() {
                pos.y = frame.height / 2 + 100
            } else {
                pos.y = -(frame.height / 2 + 100)
            }
        }
        
        return pos
    }
    
    
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}