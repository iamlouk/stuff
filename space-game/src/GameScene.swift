//
//  GameScene.swift
//  Space Game
//
//  Created by Lou Knauer on 18.10.15.
//  Copyright (c) 2015 LouK. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let spaceship:Spaceship = Spaceship()
    
    var screen:(maxX: CGFloat, minX: CGFloat, maxY: CGFloat, minY: CGFloat) = (0, 0, 0, 0)
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
//        let myLabel = SKLabelNode(fontNamed:"Chalkduster")
//        myLabel.text = "Hello, World!";
//        myLabel.fontSize = 45;
//        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame));
//        
//        self.addChild(myLabel)
        
        
        screen.maxX = (self.frame.width / 2)
        screen.minX = -(self.frame.width / 2)
        screen.maxY = (self.frame.height / 2)
        screen.minY = -(self.frame.height / 2)
        
        spaceship.position.x = screen.minX / 2
        spaceship.position.y = 0
        
//        print("maxX: ", screen.maxX)
//        print("maxY: ", screen.maxY)
//        print("minX: ", screen.minX)
//        print("minY: ", screen.minY)
        
        self.physicsWorld.contactDelegate = self
        self.backgroundColor = NSColor(red: 0.1, green: 0.1, blue: 0.2, alpha: 1)
        self.addChild(spaceship)
        
        
        
        NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: Selector("spawnNewEnemy"), userInfo: nil, repeats: true)
    }
    
    
    
    func spawnNewEnemy() {
        let enemy = Enemy()
        self.addChild(enemy)
        enemy.fire(self.frame, callback: {
            _ in
            //...
        })
    }
    
    
    
    override func mouseDown(theEvent: NSEvent) {
        /* Called when a mouse click occurs */
        
        let location = theEvent.locationInNode(self);
        print("X: \(location.x) | Y: \(location.y)");
    }
    
    override func mouseUp(theEvent: NSEvent) {
        
    }
    
    var keyMap = (up: false, down: false, left: false, right: false, spacebar: false)
    
    override func keyDown(theEvent: NSEvent) {
        switch theEvent.keyCode {
            /* Pfeil oben    */ case 126:   keyMap.up = true;
            /* Pfeil unten   */ case 125:	keyMap.down = true;
            /* Pfeil rechts  */ case 124:	keyMap.right = true;
            /* Pfeil links   */ case 123:	keyMap.left = true;
            /* Leertaste     */ case 49:    keyMap.spacebar = true; shoot();
        default:   break;
        }
    }
    
    override func keyUp(theEvent: NSEvent) {
        switch theEvent.keyCode {
            /* Pfeil oben    */ case 126:   keyMap.up = false;
            /* Pfeil unten   */ case 125:	keyMap.down = false;
            /* Pfeil rechts  */ case 124:	keyMap.right = false;
            /* Pfeil links   */ case 123:	keyMap.left = false;
            /* Leertaste     */ case 49:    keyMap.spacebar = false;
        default:   break;
        }
    }
    
    
    
    /* Rockets... */
    
    var rockets:[Rocket] = []
    
    func shoot(){
        let rocket = Rocket(direction: spaceship.direction, pos: spaceship.position)
        self.addChild(rocket)
        rockets.append(rocket)
        rocket.fire({
            (rocket: Rocket) -> Void in
            
            let index = self.rockets.indexOf( rocket )!
            self.rockets.removeAtIndex(index)
        })
    }
    
    /* Rockets... end! */
    
    
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
        if (keyMap.up && spaceship.position.y < screen.maxY) {
            if (keyMap.right && spaceship.position.x < screen.maxX) {
                spaceship.moveShipUpRight()
                return
            }
            if (keyMap.left && spaceship.position.x > screen.minX) {
                spaceship.moveShipUpLeft()
                return
            }
            
            spaceship.moveShipUp()
            return
        }
        
        
        if (keyMap.down && spaceship.position.y > screen.minY) {
            if (keyMap.right && spaceship.position.x < screen.maxX) {
                spaceship.moveShipDownRight()
                return
            }
            if (keyMap.left && spaceship.position.x > screen.minX) {
                spaceship.moveShipDownLeft()
                return
            }
            
            spaceship.moveShipDown()
            return
        }
        
        if (keyMap.right && spaceship.position.x < screen.maxX) {
            spaceship.moveShipRight()
            return
        }
        if (keyMap.left && spaceship.position.x > screen.minX) {
            spaceship.moveShipLeft()
            return
        }

    }
}
