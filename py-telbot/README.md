# Python Telegram Bot (for personal use)

[@Lou\_made\_a\_Bot](http://t.me/Lou_made_a_Bot)

- `app/config.sh` should export Bot Token, Openweathermap Api Key, etc...
- Run Bot locally: `python app/run.sh` (`python3.7` or higher)

## Create Docker-Image:

```
# use Makefile:
make

# or:

docker build --tag pytelbot .
docker run pytelbot
```

- Create Image: `make lous-pytelbot.tar.gz`
- Load Image: `zcat lous-pytelbot.tar.gz | docker load`

## Example `app/config.sh`

```
export TELEGRAM_BOT_TOKEN="..."
export OPENWEATHERMAP_API_KEY="..."

```
