# -*- coding: UTF-8 -*-

import os
import requests
from random import random
from telegram import Update
from telegram.ext import MessageHandler, Filters, CallbackContext
from telegram.ext.dispatcher import run_async




def to_celsius(kelvin):
	return round(kelvin - 273.15, 2)

@run_async
def on_location(update: Update, context: CallbackContext):
	bot = context.bot
	pos = update.message.location
	url = "http://api.openweathermap.org/data/2.5/weather?APPID=%s&lon=%s&lat=%s" % (os.getenv('OPENWEATHERMAP_API_KEY'), pos.longitude, pos.latitude)
	r = requests.get(url)
	if r.status_code != requests.codes.ok:
		bot.send_message(chat_id=update.message.chat_id, text="Failed to fetch weather information :(")
		return

	data = r.json()

	text = "\n".join([
		"Wetter in {} ({}) ⛅".format(data['name'], data['sys']['country']),
		"Temperatur: {} - {} °C 🌡".format(to_celsius(data['main']['temp_min']), to_celsius(data['main']['temp_max'])),
		"{} m/s Windgeschwindigkeit".format(data['wind']['speed']),
		"{} % Wolken".format(data['clouds']['all'])])

	bot.send_message(chat_id=update.message.chat_id, text=text)

location_handler = MessageHandler(Filters.location, on_location)
