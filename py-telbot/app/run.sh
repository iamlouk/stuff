#!/bin/bash

cd "$(dirname "$(readlink -f $0)")"

source ./config.sh

LOGFILE="logs/log.txt"
DATE="$(date '+%d.%m.%Y %H:%M:%S')"

# ps aux | grep __main__.py | grep -v grep > /dev/null
# if [ $? -eq 0 ]; then
#	exit 1
# fi

echo "-------- Starting new Bot-Instance ($DATE) --------"

exec python3 ${PYFLAGS} __main__.py # >> $LOGFILE 2>&1
