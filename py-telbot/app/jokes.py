# -*- coding: UTF-8 -*-

import requests
from random import random
from telegram import Update
from telegram.ext import CommandHandler, CallbackContext
from telegram.ext.dispatcher import run_async


def get_dad_joke():
    r = requests.get('https://icanhazdadjoke.com/', headers={'Accept':'application/json'})
    data = r.json()
    # print(data)
    return str(data['joke'])

def get_chuck_norris_joke():
    data = requests.get('http://api.icndb.com/jokes/random?escape=javascript').json()
    joke = data['value']['joke'].replace('\\"', '"').replace("\\'", "'")
    return str(joke)

@run_async
def on_joke(update: Update, context: CallbackContext):
    bot = context.bot
    joke = get_dad_joke() if random() < 0.5 else get_chuck_norris_joke()
    bot.send_message(chat_id=update.message.chat_id, text=joke)


joke_handler = CommandHandler('joke', on_joke)
