import os
import json

"""
import MySQLdb

db = MySQLdb.connect(
	host=os.getenv("MYSQLDB_HOST"),
	db=os.getenv("MYSQLDB_DB"),
	user=os.getenv("MYSQLDB_USER"),
	passwd=os.getenv("MYSQLDB_PW")
)

c = db.cursor()
c.execute("describe users")

print(c.fetchall())
"""


class JSONDataStore(object):
	def __init__(self, filename):
		self.filename = filename
		self.data = {}

	def load(self):
		if not os.path.isfile(self.filename):
			return False
		f = open("./data/{}.json".format(self.filename), 'r')
		self.data = json.load(f)
		f.close()
		return True
	
	def save(self):
		f = open("./data/{}.json".format(self.filename), 'w')
		json.dump(self.data, f, indent=2)
		f.close()


class User(JSONDataStore):
	def __init__(self, id, username=None, first_name=None, last_name=None):
		super(User, self).__init__("user_{}".format(id))
		self.id = id
		self.username = username
		self.first_name = first_name
		self.last_name = last_name
		if self.load():
			self.username = self.data['username']
			self.first_name = self.data['first_name']
			self.last_name = self.data['last_name']
			self.stored = True
		else:
			self.stored = False

	def is_admin(self):
		return self.username == 'lou_k'

	def save(self):
		self.data['username'] = self.username
		self.data['first_name'] = self.first_name
		self.data['last_name'] = self.last_name
		super(User, self).save()



