let ctx = canvas.getContext('2d');

const CELLS_X = Math.floor(canvas.width / 10);

const CELL_WIDTH = Math.floor(canvas.width / CELLS_X);
const CELL_HEIGHT = CELL_WIDTH;
const CELLS_Y = Math.floor( canvas.height / CELL_HEIGHT );

ctx.fillStyle = 'red';
ctx.fillRect(0, CELLS_Y * CELL_HEIGHT, canvas.width, CELL_HEIGHT);

const WIDTH = CELLS_X+1, HEIGHT = CELLS_Y;

const BERRY_COLOR = 'rgb(242, 207, 21)';
const SPEED = 100;

const calcDistance = (a, b) => Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));

class Cell {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    draw(){
        ctx.fillRect(this.x * CELL_WIDTH, this.y * CELL_HEIGHT, CELL_WIDTH, CELL_HEIGHT);
    }

    clear(){
        ctx.clearRect(this.x * CELL_WIDTH, this.y * CELL_HEIGHT, CELL_WIDTH, CELL_HEIGHT);
    }

}

// A Berry,
// TODO: Special Berrys like 'Speedup'
class Berry extends Cell {
    constructor(x, y){
        super(x, y);
    }
}

class Snake {
    constructor(x, y, color, initialLength = 7) {
        this.id = color;
        this.pos = { x: x, y: y };
        this.cells = [];
        for (var i = 0; i < initialLength; i++)
        this.cells.push( new Cell(x, y) );

        this.vec = { x: 0, y: 0 };
        this.color = color;
    }

    update(){
        let newx = this.pos.x + this.vec.x,
        newy = this.pos.y + this.vec.y;

        if (newx >= WIDTH) newx = 0;
        else if (newx < 0) newx = WIDTH - 1;

        if (newy >= HEIGHT) newy = 0;
        else if (newy < 0) newy = HEIGHT - 1;

        this.pos.x = newx;
        this.pos.y = newy;

        if (!(this.vec.x == 0 && this.vec.y == 0))
        for (var i = 0; i < this.cells.length; i++)
        if (this.cells[i].x == newx && this.cells[i].y == newy)
        this.snakeHitItself();

        let cell = this.cells.pop();
        cell.clear();
        cell.x = this.pos.x;
        cell.y = this.pos.y;
        this.cells.unshift(cell);
    }

    // Override this method
    snakeHitItself(){
        console.error();
    }

    // Override this method
    hitSnake(otherSnake){
        console.error();
    }

    eat(berry, index){
        berry.clear();
        berrys.berrys.splice(index, 1);
        let lastCell = this.cells[this.cells.length-1];
        this.cells.push( new Cell(lastCell.x, lastCell.y) );
        ctx.fillStyle = this.color;
        this.cells[0].draw();
        berrys.spawn();
    }
}

class HumanSnake extends Snake {
    constructor(x, y, initialLength) {
        super(x, y, '#bcff00', initialLength);
        this.dir = -1;
        this.dirs = [
            { x:  0, y: -1 },
            { x:  1, y:  0 },
            { x:  0, y:  1 },
            { x: -1, y:  0 }
        ];
    }

    snakeHitItself(){
        gameOver(this);
    }

    hitSnake(otherSnake){
        gameOver(this);
    }

    update(){
        if (controls.turnLeft) {
            if (this.dir == -1) this.dir = 0;

            this.dir -= 1;
            if (this.dir < 0) this.dir = 3;

            this.vec = this.dirs[this.dir];
        }
        if (controls.turnRight) {
            if (this.dir == -1) this.dir = 0;

            this.dir += 1;
            if (this.dir > 3) this.dir = 0;

            this.vec = this.dirs[this.dir];
        }

        controls.update();
        super.update();
    }
}

class RoboSnake extends Snake {

    constructor(color, x, y) {
        super(x, y, color);
        this.target = { x: this.x, y: this.y };
    }

    selectTarget(){
        let selectedBerry = berrys.berrys[0];
        let distance = calcDistance(this.pos, selectedBerry);
        for (let berry of berrys.berrys) {
            let dist = calcDistance(this.pos, berry);
            if (dist < distance) {
                selectedBerry = berry;
                distance = dist;
            }
        }

        this.target.x = selectedBerry.x;
        this.target.y = selectedBerry.y;
    }

    selectRandomTarget(){
        let index = Math.floor(Math.random() * berrys.berrys.length);
        let berry = berrys.berrys[index];
        this.target.x = berry.x;
        this.target.y = berry.y;
    }

    checkTarget(){
        if (this.pos.x == this.target.x && this.pos.y == this.target.y)
        return this.selectTarget();

        let exists = false;
        for (let berry of berrys.berrys) {
            if (berry.x == this.target.x && berry.y == this.target.y) {
                exists = true;
                break;
            }
        }

        if (exists == false)
        this.selectTarget();
    }

    calcVecYX(){
        let vec = { x: 0, y: 0 };

        if (this.target.y > this.pos.y)
        vec.y = 1;
        else if (this.target.y < this.pos.y)
        vec.y = -1;
        else if (this.target.x > this.pos.x)
        vec.x = 1;
        else if (this.target.x < this.pos.x)
        vec.x = -1;

        return vec;
    }

    calcVecXY(){
        let vec = { x: 0, y: 0 };

        if (this.target.x > this.pos.x)
        vec.x = 1;
        else if (this.target.x < this.pos.x)
        vec.x = -1;
        else if (this.target.y > this.pos.y)
        vec.y = 1;
        else if (this.target.y < this.pos.y)
        vec.y = -1;

        return vec;
    }

    calcVec(){
        let a = this.calcVecXY();
        if (!this.willHitSomething(a))
        return (this.vec = a);

        let b = this.calcVecYX();
        if (!this.willHitSomething(b))
        return (this.vec = b);

        for (var i = 0; i < 4; i++) {
            let vec = { x: 0, y: 0 };
            if (i == 0) {
                vec.x = 1;
                vec.y = 0;
            } else if (i == 1) {
                vec.x = -1;
                vec.y = 0;
            } else if (i == 2) {
                vec.x = 0;
                vec.y = 1;
            } else if (i == 3) {
                vec.x = 0;
                vec.y = -1;
            }
            if (!this.willHitSomething(vec))
            return (this.vec = vec);
        }

        console.warn("Kein Weg gefunden :(");
        this.vec = { x: 0, y: 0 };
        this.update = () => {};
    }

    willHitSomething(vec){
        let x = this.pos.x + vec.x;
        let y = this.pos.y + vec.y;
        for (let snake of snakes) {
            for (let cell of snake.cells) {
                if (cell.x == x && cell.y == y)
                return true;
            }
        }
        return false;
    }

    update(){
        this.checkTarget();

        this.calcVec();

        super.update();
    }

    hitSnake(otherSnake){
        this.vec = { x: 0, y: 0 };
        this.update = () => {};
        console.warn('Snake#'+this.id+' died!');
    }

}


let berrys = ({

    berrys: [],

    spawn: function(){
        let x = 4+Math.floor(Math.random() * (WIDTH-8));
        let y = 4+Math.floor(Math.random() * (HEIGHT-8));

        for (let snake of snakes) {
            for (let cell of snake.cells) {
                if (x == cell.x && y == cell.y)
                return this.spawn();
            }
        }

        for (let berry of this.berrys) {
            if (berry.x == x && berry.y == y)
            return this.spawn();
        }

        let berry = new Berry(x, y);
        ctx.fillStyle = BERRY_COLOR;
        berry.draw();
        this.berrys.push( berry );
    },

});





let snakes = [
    new HumanSnake(Math.floor(WIDTH / 3), Math.floor(HEIGHT / 2)),
    // new RoboSnake('rgb(255, 0, 0)', Math.floor(WIDTH / 3) * 2, Math.floor(HEIGHT / 2)),
];


for (let i = 0; i<5; i++)
    berrys.spawn();

let paused = false;
let onframe = () => {
    if (paused === false) {
        // console.log('Tick!');
        for (let snake of snakes) {
            snake.update();

            ctx.fillStyle = snake.color;
            snake.cells[0].draw();

            for (let i = 0; i<berrys.berrys.length; i++) {
                let berry = berrys.berrys[i];
                if (berry.x === snake.pos.x && berry.y === snake.pos.y)
                snake.eat(berry, i);
            }

            for (let _snake of snakes) {
                if (_snake.id !== snake.id) {
                    for (let cell of _snake.cells) {
                        if (snake.pos.x === cell.x && snake.pos.y === cell.y)
                        snake.hitSnake(_snake);
                    }
                }
            }
        }
    }
};

setInterval(onframe, SPEED);

let gameOver = (snake) => {
    gameOver = () => {};
    let score = snake.cells.length * (1 / SPEED * 1000);
    paused = true;
    console.warn('to be continued...');
    document.body.innerHTML = '<h2>Game Over! ('+score+')</h2>';
};
