

let controls = ({


    turnLeft: false,
    turnRight: false,
    nextTurnLeft: false,
    nextTurnRight: false,

    init: function(){
        document.querySelector('.btn#left').addEventListener('click', event => this.leftPressed(), false);
        document.querySelector('.btn#right').addEventListener('click', event => this.rightPressed(), false);

        document.addEventListener('keydown', event => {
            if (event.keyCode == 37) this.leftPressed();
            if (event.keyCode == 39) this.rightPressed();
        }, false);

        return this;
    },

    leftPressed: function(){
        // console.log('<');
        if (this.turnLeft || this.turnRight) {
            this.nextTurnLeft = true;
            this.nextTurnRight = false;
            return;
        }

        this.turnLeft = true;
        this.turnRight = false;
    },

    rightPressed: function(){
        // console.log('>');
        if (this.turnLeft || this.turnRight) {
            this.nextTurnLeft = false;
            this.nextTurnRight = true;
            return;
        }

        this.turnLeft = false;
        this.turnRight = true;
    },

    update: function(){
        this.turnLeft = this.nextTurnLeft;
        this.turnRight = this.nextTurnRight;
        this.nextTurnLeft = false;
        this.nextTurnRight = false;
    }


}).init();


let query = {};
    (window.location.href.toString().split('?')[1] || '')
    .split('#')[0]
    .split('&')
    .map(e => e.split('='))
    .forEach((pair) => query[pair[0]] = pair[1]);

