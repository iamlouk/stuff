# -*- coding: UTF-8 -*-

import os, subprocess
import requests
from random import random
from telegram import Update, ParseMode
from telegram.ext import CommandHandler, Filters, CallbackContext
from telegram.ext.dispatcher import run_async

@run_async
def on_btc(update: Update, context: CallbackContext):
	bot = context.bot
	r = requests.get('http://blockchain.info/de/ticker')
	if r.status_code != requests.codes.ok:
		bot.send_message(chat_id=update.message.chat_id, text="Failed to fetch requested information :(")
		return

	data = r.json()
	bot.send_message(chat_id=update.message.chat_id, text=("1 BTC = %s € / %s $" % (data['EUR']['last'], data['USD']['last'])))

btc_handler = CommandHandler('btc', on_btc)

@run_async
def on_cat(update: Update, context: CallbackContext):
	bot = context.bot
	bot.sendChatAction(chat_id=update.message.chat_id, action="upload_photo")
	url = "http://aws.random.cat/meow"
	r = requests.get(url).json()
	if "file" in r:
		bot.send_photo(chat_id=update.message.chat_id, photo=r["file"])
	else:
		bot.send_message(chat_id=update.message.chat_id, text=f"Error :(\n{r}")

cat_handler = CommandHandler("cat", on_cat)

@run_async
def on_ping(update: Update, context: CallbackContext):
	args = context.args
	bot = context.bot

	if len(args) > 0:
		for arg in args:
			res = subprocess.run(['ping',  '-c', '1', '-w', '1', arg], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			stdout = res.stdout.decode('UTF-8')
			stderr = res.stderr.decode('UTF-8')
			bot.send_message(chat_id=update.message.chat_id, text=f"```{(stdout)}{(stderr)}```", parse_mode=ParseMode.MARKDOWN)
	else:
		print('Ping! chat_id={}'.format(update.message.chat_id))
		bot.send_message(chat_id=update.message.chat_id, text="Pong!  🏓  ")

ping_handler = CommandHandler('ping', on_ping)
