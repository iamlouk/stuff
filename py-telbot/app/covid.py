# -*- coding: UTF-8 -*-

import requests
from random import random
from telegram import Update
from telegram.ext import CommandHandler, CallbackContext
from telegram.ext.dispatcher import run_async

from bs4 import BeautifulSoup

def get_cases():
    html = requests.get("https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_pandemic_by_country_and_territory#/").text
    soup = BeautifulSoup(html, 'html.parser')
    table = soup.find('table', attrs={ 'class': 'wikitable' })
    rows = table.find('tbody').find_all('tr')

    cases = []
    for idx, row in enumerate(rows):
        if idx < 3:
            continue
        if idx > 15:
            break

        country = row.find_all('th')[1].get_text().split('[')[0]
        ncases = row.find_all('td')[0].string

        cases.append((country.strip(), ncases.strip().replace(',', '.')))


    cases = map(lambda tpl: f"{tpl[0]}: {tpl[1]}", cases)
    cases = ', '.join(cases)
    return f"Covid-19 Fallzahlen: {cases}, ..."

@run_async
def on_covid_cmd(update: Update, context: CallbackContext):
    bot = context.bot
    text = get_cases()
    bot.send_message(chat_id=update.message.chat_id, text=text)

if __name__ == '__main__':
    print(get_cases())

