# -*- coding: UTF-8 -*-

import re
import requests
from random import random
from telegram import Update
from telegram.ext import CommandHandler, CallbackContext
from telegram.ext.dispatcher import run_async
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

def extract_food(text, essen, i0=0):
	istart = text.find(essen, i0)
	if istart < 0:
		return None

	i1 = text.find("</br>", istart) + 5
	i2 = text.find("</br>", i1) + 5
	i3 = text.find("</br>", i2) + 5

	food = [
		text[istart:(i1 - 5)],
		text[i1:(i2 - 5)],
		text[i2:(i3 - 5)]
	]

	food[1] = food[1].replace('<sup><b>', '').replace('</b></sup>', '')

	if food[2].find("Wahlbeilagen") != 0:
		food = u"{}: {}".format(food[0], food[1])
	else:
		food = u"{}: {}\n{}".format(food[0], food[1], food[2])

	return re.sub(r'(\([^)]*)\)', '', food)


def extract_all(title, url):
	text = str(requests.get(url).text)
	i0 = text.find("Speiseplan <br><h4>")
	if i0 < 0:
		return "i0 nicht gefunden :("

	i0 += 19
	date = text[i0:text.find("</h4>", i0)]

	food = []
	food.append(extract_food(text, "Essen:", i0))
	for i in range(1, 10):
		food.append(extract_food(text, "Essen {}".format(i), i0))

	food.append(extract_food(text, "Aktionsessen:", i0))
	for i in range(1, 10):
		food.append(extract_food(text, "Aktionsessen {}".format(i), i0))

	food = list(filter(lambda x: x != None, food))
	food.insert(0, "{} ({}):".format(title, date))
	return '\n'.join(food)



def on_mensa_cmd(update: Update, context: CallbackContext):
	bot = context.bot
	keyboard = [[
		InlineKeyboardButton("Südmensa", callback_data="mensa:sued")
	], [
		InlineKeyboardButton("Langemarckplatz", callback_data="mensa:lmpl")
	]]
	bot.send_message(chat_id=update.message.chat_id, text="Welche Mensa meinst du?", reply_markup=InlineKeyboardMarkup(keyboard))



def sued():
	return extract_all("Suedmensa", "http://www.werkswelt.de/index.php?id=sued")


def lmpl():
	return extract_all("Langemarckplatz", "http://www.werkswelt.de/index.php?id=lmpl")
