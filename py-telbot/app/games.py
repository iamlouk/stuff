import logging
from telegram import Update
from telegram.ext import CommandHandler, CallbackContext
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

"""
def snake(bot, update):
	url = "http://loukna.de/telegram/snake/index.html?userId={}&chatId={}".format(update.callback_query.from_user.id, update.callback_query.message.chat_id)
	update.callback_query.answer(text="Snake!", url=url, game_short_name="Snake")

def game_query(short_name, bot, update):
	if short_name == "Snake":
		snake(bot, update)
	else:
		logging.info("game_query with unkown game!")


def on_games(bot, update):
	keyboard = [[
		InlineKeyboardButton("Snake", callback_data="game:snake")
	]]
	bot.send_message(chat_id=update.message.chat_id, text="Welches Spiel willst du spielen?", reply_markup=InlineKeyboardMarkup(keyboard))
"""



def game_query(short_name, bot, update):
	if short_name == "Snake":
                raise Exception('game_query', 'this game is not support in this version of this bot')
	else:
		logging.info("game_query with unkown game!")


def on_games(update: Update, context: CallbackContext):
	bot = context.bot
	keyboard = [[
		InlineKeyboardButton("Snake", callback_data="game:snake")
	]]
	bot.send_message(chat_id=update.message.chat_id, text="Welches Spiel willst du spielen?", reply_markup=InlineKeyboardMarkup(keyboard))

games_handler = CommandHandler('games', on_games)
