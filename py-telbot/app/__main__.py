#!/usr/bin/python3
# -*- coding: UTF-8 -*-

print("Hello, World!")

import re
import os
import sys
import logging
import requests
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler, CallbackContext
from telegram.ext.dispatcher import run_async
from telegram.error import TelegramError, TimedOut

import db

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger('pybot')

updater = Updater(token=os.getenv('TELEGRAM_BOT_TOKEN'), workers=8, use_context=True)
dispatcher = updater.dispatcher

HELP = """
Hello, Human!
/covid - List Covid-19 Cases by Country
/start - Get a nice welcome message
/help - shows this message
/ping [hostname] - Pong! 🏓
/btc - Bitcoin/€ exchange rate 💰
/joke - get a random joke
/mensa - food plan at Suedmensa and Langemarck Mensa (Erlangen)
/games - A list of games to play
/cat - random cat image
Location - Weather at this location 🌞
"""


def start_handler(update: Update, context: CallbackContext):
	bot = context.bot
	user = update.message.from_user
	logger.info('/start by {}: {} {} (Chat: {})'.format(user.id, user.first_name, user.last_name, update.message.chat_id))
	bot.send_message(chat_id=update.message.chat_id, text="Hello {} 🤠,   I'm a bot 🤖 , you should be a human. Try out /help!".format(user.first_name))
	user = db.User(user.id, username=user.username, first_name=user.first_name, last_name=user.last_name)
	user.save()

updater.dispatcher.add_handler(CommandHandler('start', start_handler))

def help_handler(update: Update, context: CallbackContext):
	bot = context.bot
	bot.send_message(chat_id=update.message.chat_id, text=HELP)

updater.dispatcher.add_handler(CommandHandler('help', help_handler))



from jokes import joke_handler
updater.dispatcher.add_handler(joke_handler)

from location import location_handler
updater.dispatcher.add_handler(location_handler)

import mensa
updater.dispatcher.add_handler(CommandHandler('mensa', mensa.on_mensa_cmd))

import covid
updater.dispatcher.add_handler(CommandHandler('covid', covid.on_covid_cmd))

import games
updater.dispatcher.add_handler(games.games_handler)

def on_callback_query(update: Update, context: CallbackContext):
	bot = context.bot
	if update.callback_query.game_short_name != None:
		games.game_query(update.callback_query.game_short_name, bot, update)
		return

	data = update.callback_query.data
	if data == 'mensa:sued':
		text = mensa.sued()
		update.callback_query.answer(text="Suedmensa:")
		bot.send_message(chat_id=update.callback_query.message.chat_id, text=text)
	elif data == 'mensa:lmpl':
		text = mensa.lmpl()
		update.callback_query.answer(text="Langemarckplatz:")
		bot.send_message(chat_id=update.callback_query.message.chat_id, text=text)
	elif data == 'game:snake':
		bot.send_game(chat_id=update.callback_query.message.chat_id, game_short_name="Snake")
	else:
		update.callback_query.answer(text="Unkown/Unexpected: {}".format(data))

updater.dispatcher.add_handler(CallbackQueryHandler(on_callback_query))

import stuff
updater.dispatcher.add_handler(stuff.cat_handler)
updater.dispatcher.add_handler(stuff.btc_handler)
updater.dispatcher.add_handler(stuff.ping_handler)

def tag_handler(update: Update, context: CallbackContext):
	bot = context.bot
	bot.send_message(chat_id=update.message.chat_id, text="Mein Meister will nicht getaggt werden! 😡 ")
	update.message.delete()

updater.dispatcher.add_handler(MessageHandler(Filters.regex(r".*@lou_k.*"), tag_handler))

# def mac_handler(update: Update, context: CallbackContext):
#	bot = context.bot
# 	bot.send_message(chat_id=update.message.chat_id, text="Macs sind toll!")

# updater.dispatcher.add_handler(RegexHandler(r"(mac(\s|$).*|Mac(\s|$).*|.*\s(mac|Mac|)\s.*)", mac_handler))


def unknown_handler(update: Update, context: CallbackContext):
	bot = context.bot
	if update.message.chat.type == 'private':
		bot.send_message(chat_id=update.message.chat_id, text="-> /help")



def new_members_handler(update: Update, context: CallbackContext):
	bot = context.bot
	# logger.info('NEW_MEMBERS_HANDLER: {}'.format(update))

	new_chat_members = update.message.new_chat_members
	if update.message.chat_id == -1001201573953:
		# logger.info('{}'.format(new_chat_members))
		if new_chat_members != None:
			for user in new_chat_members:
				if user.username == "lou_k":
					logger.info('lou_k gekickt');
					bot.kickChatMember(chat_id=update.message.chat_id, user_id=user.id)




def text_handler(update: Update, context: CallbackContext):
	bot = context.bot
	if update.message.chat.type == 'private':
		bot.send_message(chat_id=update.message.chat_id, text="-> /help")
	elif update.message.from_user.is_bot: # Bots kriegen keine anderen Bot Nachrichten :(
		bot.send_message(chat_id=update.message.chat_id, text="Hallo {}! 🤖 \nBots sind toll!! 👍 ".format(update.message.from_user.username))

updater.dispatcher.add_handler(MessageHandler(Filters.status_update.new_chat_members, new_members_handler))
updater.dispatcher.add_handler(MessageHandler(Filters.command, unknown_handler))
updater.dispatcher.add_handler(MessageHandler(Filters.text, text_handler))



def error_callback(update, context):
	print(f"Error while processing: {update}", file=sys.stderr)
	pass

updater.dispatcher.add_error_handler(error_callback)




# def spamer(bot, job):
#	bot.send_message(chat_id=244216781, text="Hoer auf zu Shitposten!")
# updater.job_queue.run_repeating(spamer, interval=10)




logging.info('Bot running and polling')
updater.start_polling()
updater.idle()
