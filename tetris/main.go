package main

import (
	"fmt"
	"os"
	"time"
	"math/rand"
	"github.com/nsf/termbox-go"
)

const (
	BOX = '█'
	BORDER1 = '╔'
	BODRER2 = '╗'
	BORDER3 = '╚'
	BORDER4 = '╝'
	BORDER5 = '═'
	BORDER6 = '║'
)

const (
	UP int = iota
	LEFT
	RIGHT
	DOWN
	QUIT
	PAUSE
)

type Matrix [][]termbox.Attribute

var termwidth int
var termheight int

var matrix Matrix
var width int
var height int

var leftpadding int

var block Matrix
var block_x int
var block_y int

func NewMatrix(w, h int) Matrix {
	m := make([][]termbox.Attribute, w)
	for i := 0; i < w; i++ {
		m[i] = make([]termbox.Attribute, h)
	}
	return m
}

func Rotate3x3Matrix(m Matrix) {
	tmp := m[0][0]
	m[0][0] = m[0][2]
	m[0][2] = m[2][2]
	m[2][2] = m[2][0]
	m[2][0] = tmp

	tmp = m[1][0]
	m[1][0] = m[0][1]
	m[0][1] = m[1][2]
	m[1][2] = m[2][1]
	m[2][1] = tmp
}

func NewBlockType1() Matrix {
	m := NewMatrix(3, 3)
	m[1][0] = termbox.ColorBlue
	m[1][1] = termbox.ColorBlue
	m[1][2] = termbox.ColorBlue
	m[2][2] = termbox.ColorBlue
	return m
}
func NewBlockType2() Matrix {
	m := NewMatrix(3, 3)
	m[1][0] = termbox.ColorGreen
	m[1][1] = termbox.ColorGreen
	m[1][2] = termbox.ColorGreen
	m[0][2] = termbox.ColorGreen
	return m
}
func NewBlockType3() Matrix {
	m := NewMatrix(3, 3)
	m[0][0] = termbox.ColorYellow
	m[0][1] = termbox.ColorYellow
	m[1][0] = termbox.ColorYellow
	m[1][1] = termbox.ColorYellow
	return m
}
func NewBlockType4() Matrix {
	m := NewMatrix(3, 3)
	m[0][1] = termbox.ColorCyan
	m[1][1] = termbox.ColorCyan
	m[1][2] = termbox.ColorCyan
	m[2][2] = termbox.ColorCyan
	return m
}
func NewBlockType5() Matrix {
	m := NewMatrix(3, 3)
	m[0][2] = termbox.ColorRed
	m[1][2] = termbox.ColorRed
	m[1][1] = termbox.ColorRed
	m[2][1] = termbox.ColorRed
	return m
}
func NewBlockType6() Matrix {
	m := NewMatrix(3, 3)
	m[0][1] = termbox.ColorMagenta
	m[1][1] = termbox.ColorMagenta
	m[2][1] = termbox.ColorMagenta
	m[1][2] = termbox.ColorMagenta
	return m
}
func NewBlockType7() Matrix {
	m := NewMatrix(3, 3)
	m[0][0] = termbox.ColorYellow
	m[0][1] = termbox.ColorYellow
	m[0][2] = termbox.ColorYellow
	m[1][1] = termbox.ColorYellow
	m[2][1] = termbox.ColorYellow
	return m
}
func NewBlockType8() Matrix {
	m := NewMatrix(3, 3)
	m[0][1] = termbox.ColorCyan
	m[1][1] = termbox.ColorCyan
	m[2][1] = termbox.ColorCyan
	return m
}
func NewBlockType9() Matrix {
	m := NewMatrix(3, 3)
	m[1][0] = termbox.ColorGreen
	m[1][1] = termbox.ColorGreen
	m[1][2] = termbox.ColorGreen
	return m
}

func NewBlock() {
	var m Matrix
	blockType := rand.Int() % 9
	switch blockType {
	case 0:
		m = NewBlockType1()
	case 1:
		m = NewBlockType2()
	case 2:
		m = NewBlockType3()
	case 3:
		m = NewBlockType4()
	case 4:
		m = NewBlockType5()
	case 5:
		m = NewBlockType6()
	case 6:
		m = NewBlockType7()
	case 7:
		m = NewBlockType8()
	case 8:
		m = NewBlockType9()
	}

	block = m
	block_x, block_y = width / 2, 0
}

func ShouldStop() bool {
	for x := 0; x < 3; x++ {
		for y := 0; y < 3; y++ {
			if col := block[x][y]; col != termbox.ColorDefault {
				rx, ry := block_x + x, block_y + y
				if ry == height - 1 {
					return true
				}
				if col := matrix[rx][ry + 1]; col != termbox.ColorDefault {
					return true
				}
			}
		}
	}
	return false
}

func DoesOverlap() bool {
	for x := 0; x < 3; x++ {
		for y := 0; y < 3; y++ {
			if col := block[x][y]; col != termbox.ColorDefault {
				rx, ry := block_x + x, block_y + y
				if ry >= height || rx < 0 || rx >= width {
					return true
				}
				if col := matrix[rx][ry]; col != termbox.ColorDefault {
					return true
				}
			}
		}
	}
	return false
}

func RenderCell(x, y int, col termbox.Attribute) {
	termbox.SetCell(leftpadding + x * 2,     y, BOX, col, termbox.ColorDefault)
	termbox.SetCell(leftpadding + x * 2 + 1, y, BOX, col, termbox.ColorDefault)
}

func ClearCell(x, y int) {
	termbox.SetCell(leftpadding + x * 2,     y, ' ', termbox.ColorDefault, termbox.ColorDefault)
	termbox.SetCell(leftpadding + x * 2 + 1, y, ' ', termbox.ColorDefault, termbox.ColorDefault)
}

func Render(borderColor termbox.Attribute) {
	if err := termbox.Clear(termbox.ColorDefault, termbox.ColorDefault); err != nil {
		panic(err)
	}

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			if col := matrix[x][y]; col != termbox.ColorDefault {
				RenderCell(x, y, col)
			}
		}
	}

	for x := 0; x < 3; x++ {
		for y := 0; y < 3; y++ {
			if col := block[x][y]; col != termbox.ColorDefault {
				rx, ry := x + block_x, y + block_y
				if rx >= 0 && rx < width && ry >= 0 && ry < height {
					RenderCell(rx, ry, col)
				}
			}
		}
	}

	DrawBorder(borderColor)
}

func DrawBorder(col termbox.Attribute) {
	termbox.SetCell(leftpadding - 1,         height, BORDER3, col, termbox.ColorDefault)
	termbox.SetCell(leftpadding + width * 2, height, BORDER4, col, termbox.ColorDefault)

	for i := 0; i < (width * 2); i++ {
		termbox.SetCell(i + leftpadding, height, BORDER5, col, termbox.ColorDefault)
	}

	for i := 0; i < height; i++ {
		termbox.SetCell(leftpadding - 1,         i, BORDER6, col, termbox.ColorDefault)
		termbox.SetCell(leftpadding + width * 2, i, BORDER6, col, termbox.ColorDefault)
	}
}

func HandleEvents(c chan int) {
	for {
		if event := termbox.PollEvent(); event.Type == termbox.EventKey {
			switch event.Key {
			case termbox.KeyArrowUp:
				c <- UP
			case termbox.KeyArrowDown:
				c <- DOWN
			case termbox.KeyArrowLeft:
				c <- LEFT
			case termbox.KeyArrowRight:
				c <- RIGHT
			case termbox.KeySpace:
				c <- PAUSE
			case termbox.KeyCtrlC:
				fallthrough
			case termbox.KeyEsc:
				c <- QUIT
				return
			}
			if event.Ch == 'q' {
				c <- QUIT
				return
			}
		}
	}
}

func RemoveRow(row int) {
	for y := row; y >= 0; y-- {
		for x := 0; x < width; x++ {
			col := termbox.ColorDefault
			if y != 0 {
				col = matrix[x][y - 1]
			}

			matrix[x][y] = col
		}
	}
}

func MoveBlockDown() {
	if ShouldStop() {
		for x := 0; x < 3; x++ {
			for y := 0; y < 3; y++ {
				if col := block[x][y]; col != termbox.ColorDefault {
					rx, ry := x + block_x, y + block_y
					matrix[rx][ry] = col
				}
			}
		}

		for y := 0; y < height; y++ {
			rmline := true
			for x := 0; x < width; x++ {
				if matrix[x][y] == termbox.ColorDefault {
					rmline = false
					break
				}
			}

			if rmline {
				RemoveRow(y)
			}
		}

		NewBlock()
	} else {
		block_y += 1
	}
}

func main() {
	if err := termbox.Init(); err != nil {
		panic(err)
	}

	rand.Seed(time.Now().Unix())
	termwidth, termheight = termbox.Size()

	if termwidth < 30 || termheight < 20 {
		fmt.Fprintf(os.Stderr, "terminal too small!\n")
		os.Exit(1)
	}

	width, height = 15, termheight - 1
	leftpadding = termwidth / 2 - width


	if err := termbox.Clear(termbox.ColorDefault, termbox.ColorDefault); err != nil {
		panic(err)
	}

	matrix = NewMatrix(width, height)

	NewBlock()

	DrawBorder(termbox.ColorDefault)

	actions := make(chan int, 16)
	timer := time.NewTicker(50 * time.Millisecond).C
	borderCol := termbox.ColorDefault

	go HandleEvents(actions)

	paused, didstuff := false, false
	speedCounter := 0
	i := 0

	for {
		if borderCol != termbox.ColorDefault {
			borderCol = termbox.ColorDefault
		}

		select {
		case action := <-actions:
			switch action {
			case LEFT:
				block_x -= 1
				if DoesOverlap() {
					block_x += 1
					borderCol = termbox.ColorRed
					break
				}
				didstuff = true
			case RIGHT:
				block_x += 1
				if DoesOverlap() {
					block_x -= 1
					borderCol = termbox.ColorRed
					break
				}
				didstuff = true
			case UP:
				for {
					Rotate3x3Matrix(block)
					if !DoesOverlap() {
						break
					}
				}
				didstuff = true
			case DOWN:
				speedCounter += 10
				break
			case PAUSE:
				paused = !paused
			case QUIT:
				termbox.Close()
				return
			}
		case <-timer:
			if didstuff {
				<-timer
				didstuff = false
				continue
			}
			break;
		}

		i += 1
		if speedCounter > 0 {
			speedCounter -= 1
		} else if !(i % 3 == 0) {
			continue
		}

		if paused {
			continue
		}

		if !didstuff {
			MoveBlockDown()
		}

		Render(borderCol)

		if err := termbox.Flush(); err != nil {
			panic(err)
		}
	}
}
