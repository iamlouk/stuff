import os

histfile = None

MAXSIZE = 10

history = []

changed = False

def load(hf):
	global histfile
	if histfile != None:
		return

	histfile = hf
	if os.path.isfile(histfile):
		with open(hf) as f:
			for line in f:
				history.append(line.strip())

def get():
	return history

def add(cmd):
	global history
	if not cmd in history:
		history.insert(0, cmd)
		history = history[0:MAXSIZE]
		global changed
		changed = True

def done():
	if changed:
		with open(histfile, 'w') as f:
			for line in history:
				f.write(line + '\n')



