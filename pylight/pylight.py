#!/usr/bin/env python3

import os, time, sys
import cmds
import history
import xml.etree.ElementTree as XMLTree

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


TYPE_EXECUTABLE, TYPE_RECENTLY_USED, TYPE_PATH, TYPE_PYFN = "executable", "recently used file", "path", "python function"

PATH = list(set(os.environ['PATH'].split(':')))
PYLIGHT_HOME = os.path.dirname(os.path.realpath(__file__))
PATH.append(PYLIGHT_HOME + "/scripts")

get_millis = lambda: int(round(time.time() * 1000))
os.chdir(os.environ['HOME'])

history.load(PYLIGHT_HOME + "/history.txt")

class Highlight():
	def __init__(self, name, htype, path=None, fn=None):
		self.name = name
		self.path = path
		self.htype = htype
		self.fn = fn

class Completions():
	class Node():
		def __init__(self, char, parent=None):
			self.map = dict()
			self.parent = parent
			self.hls = list()
			self.char = char

		def __str__(self):
			return "CompNode(char='%s', len(hls)=%d)" % (self.char, len(self.hls))

		def insert(self, hl, idx):
			if idx == len(hl.name):
				self.hls.append(hl)
			else:
				node = self.map.get(hl.name[idx])
				if node is None:
					node = Completions.Node(hl.name[idx], parent=self)
					self.map[hl.name[idx]] = node
				node.insert(hl, idx + 1)

		def to_list(self, l):
			for hl in self.hls:
				l.append(hl)

			for _, node in self.map.items():
				node.to_list(l)

			return l

		def get_sub_tree(self, string):
			if len(string) == 0:
				return self

			node = self.map.get(string[0])
			if node is not None:
				return node.get_sub_tree(string[1:])

			return None

	def __init__(self):
		self.root = Completions.Node("")

	def load(self):
		t1 = get_millis()
		self.add_PATH_hls()
		self.add_HOME_files()
		self.add_recently_used_files()

		for tpl in cmds.cmds:
			self.root.insert(Highlight(tpl[0], TYPE_PYFN, fn=tpl[1]), 0)

		def help(_):
			readme = PYLIGHT_HOME + "/README.md"
			start_and_disown("xdg-open " + readme)

		def quit(_):
			pass

		comps.root.insert(Highlight("quit", TYPE_PYFN, fn=quit), 0)
		comps.root.insert(Highlight("q", TYPE_PYFN, fn=quit), 0)
		comps.root.insert(Highlight("help", TYPE_PYFN, fn=help), 0)

		print("comp. build time: %dms" % (get_millis() - t1))


	def add_PATH_hls(self):
		# print("$PATH: ", PATH)
		for path in PATH:
			with os.scandir(path) as it:
				for direntry in it:
					hl = Highlight(direntry.name, TYPE_EXECUTABLE, path=direntry.path)
					self.root.insert(hl, 0)

	def add_recently_used_files(self):
		xml = XMLTree.parse("%s/.local/share/recently-used.xbel" % os.environ["HOME"])
		root = xml.getroot()
		for node in root.iter("bookmark"):
			href = node.attrib["href"]
			if href.startswith("file://"):
				path = href[7:]
				if os.path.exists(path) :
					name = os.path.basename(path)
					if len(name) == 0:
						name = "/"
					hl = Highlight(name, TYPE_RECENTLY_USED, path=path)
					self.root.insert(hl, 0)

	def add_HOME_files(self):
		with os.scandir(os.environ["HOME"]) as it:
			for direntry in it:
				if direntry.is_dir():
					hl = Highlight(direntry.name + "/", TYPE_PATH, path=direntry.path)
					self.root.insert(hl, 0)
				else:
					hl = Highlight(direntry.name, TYPE_PATH, path=direntry.path)
					self.root.insert(hl, 0)

def start_and_disown(cmd):
	os.system("%s &> /dev/null & disown" % cmd)

def start_in_terminal(cmd):
	cmd = cmd.replace("\"", "\\\"")
	# for i in 5 4 3 2 1; do printf \\">>> exits in \\$i <<<\\r\\"; sleep 1; done
	os.system('gnome-terminal -- {shell} -c "{cmd}; echo; echo +++hit enter to close terminal+++; read"'.format(shell=os.environ["SHELL"], cmd=cmd))


class PylightWindow(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self, title="Pylight")
		self.set_default_icon_from_file("%s/icon.png" % PYLIGHT_HOME)
		self.connect("destroy", Gtk.main_quit)
		self.connect("delete-event", Gtk.main_quit)
		self.set_position(Gtk.WindowPosition.CENTER_ALWAYS)
		self.set_size_request(700, 100)

		vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
		self.add(vbox)

		self.label = Gtk.Label("type 'help' for help")
		vbox.pack_start(self.label, True, True, 0)

		self.query_entry = Gtk.Entry()
		self.query_entry.set_text("")
		self.query_entry.placeholder_text = "Query..."
		self.query_entry.connect("changed", self.on_query_change)
		self.query_entry.connect("activate", self.on_enter)
		vbox.pack_start(self.query_entry, True, True, 0)

		self.liststore = Gtk.ListStore(str)
		self.completion = Gtk.EntryCompletion()
		self.completion.set_model(self.liststore)
		self.completion.set_text_column(0)
		self.completion.set_minimum_key_length(0)

		self.alt1_pressed = False
		self.alt2_pressed = False
		KEYVAL_ENTER = 65293
		KEYVAL_ALT1, KEYVAL_ALT2 = 65513, 65027

		def key_press_event(widget, event):
			if event.keyval == KEYVAL_ALT1:
				self.alt1_pressed = True
			elif event.keyval == 65307:
				self.done()
			elif event.keyval == KEYVAL_ALT2:
				self.alt2_pressed = True

		def key_release_event(widget, event):
			if event.keyval == KEYVAL_ALT1:
				self.alt1_pressed = False
			elif event.keyval == KEYVAL_ALT2:
				self.alt2_pressed = False

		self.query_entry.connect("key-press-event", key_press_event)
		self.query_entry.connect("key-release-event", key_release_event)


		self.query_entry.set_completion(self.completion)

		for hist in history.get():
			self.liststore.append((hist,))

		self.current_comp_path = None # when entering a path, this is the path that is currently completed

		#def window_event(widget, event):
		#	self.query_entry.emit('changed')
		#	self.completion.complete()
		#	print(event)
		#	print(self.liststore.get_n_items())
		#self.connect("window-state-event", window_event)


	def is_path(self, string):
		i = len(string) - 1
		path = ""
		ispath = False
		while i >= 0:
			c = string[i]
			if c == "/":
				ispath = True
				path = "/" + path
			elif c.isspace():
				break
			else:
				path = c + path
			i -= 1
		return path if ispath else None

	def on_query_change(self, widget):
		query = self.query_entry.get_text()
		path = self.is_path(query)
		if path is not None:
			if path.endswith("/") and os.path.isdir(path):
				self.current_comp_path = path[0:len(path) - 1]
				self.liststore.clear()
				with os.scandir(path) as it:
					for direntry in it:
						if direntry.is_dir():
							self.liststore.append((path + direntry.name + "/",))
						else:
							self.liststore.append((path + direntry.name,))
				self.completion.complete()
				# self.query_entry.emit('changed') # macht iwie endlosrek. :/
			elif os.path.isdir(path) and os.path.dirname(path) is self.current_comp_path:
				pass
			else:
				pass
		elif len(query) is not 0:
			self.current_comp_path = None
			self.liststore.clear()
			subtree = comps.root.get_sub_tree(query)
			if subtree is not None:
				l = subtree.to_list(list())
				for hl in l:
					if hl.htype is TYPE_EXECUTABLE or hl.htype is TYPE_PATH or hl.htype is TYPE_PYFN:
						self.liststore.append((hl.name,))
					elif hl.htype is TYPE_RECENTLY_USED:
						item = "%s (%s)" % (hl.name, hl.path)
						self.liststore.append((item,))
					else:
						print("??? no autocompletion ???")

		self.completion.complete()

	def on_enter(self, widget):
		q = self.query_entry.get_text().split(" ")
		head, tail = q.pop(0), None if len(q) == 0 else " ".join(q)

		path = self.is_path(head)
		if path is not None and tail is None:
			if not os.path.exists(path):
				self.not_found()
				return
			start_and_disown("xdg-open %s" % path)
			self.done()
			return

		subtree = comps.root.get_sub_tree(head)
		if subtree is None:
			self.not_found()
			return

		l = subtree.hls
		if tail is not None and tail.startswith("(") and tail.endswith(")"):
			l = list(filter(lambda hl: hl.path == tail[1:len(tail)-1], l))
		if len(l) == 0:
			return self.not_found()
		if len(l) > 1:
			print("warning: multiple matches")

		hl = l[0]
		if hl.htype is TYPE_PATH or hl.htype is TYPE_RECENTLY_USED:
			start_and_disown("xdg-open %s" % hl.path)
			self.done()
			return
		elif hl.htype is TYPE_PYFN:
			hl.fn(tail)
			self.done()
			return
		elif hl.htype is TYPE_EXECUTABLE:
			if self.alt1_pressed or self.alt2_pressed:
				start_in_terminal("%s %s" % (hl.path, tail if tail is not None else ""))
			else:
				start_and_disown("%s %s" % (hl.path, tail if tail is not None else ""))
			self.done()
			return

		self.not_found()

	def done(self):
		history.add(self.query_entry.get_text())
		self.query_entry.set_text("")
		self.label.set_text("")
		history.done()
		self.close()
		sys.exit(0)

	def not_found(self):
		print("warning: no matches found")
		self.label.set_markup('<span color="red">warning: no matches</span>')

	def show(self):
		self.show_all()


comps = Completions()
comps.load()

window = PylightWindow()
window.show()
window.completion.set_minimum_key_length(0)
window.query_entry.emit('changed')
# window.completion.complete()

Gtk.main()
