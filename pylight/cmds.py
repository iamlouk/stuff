import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


def hello(name):
	title = "Hello, %s!" % ("World" if name is None else name)
	dialog = Gtk.MessageDialog(title=title)
	dialog.add_button("Ok", 0)
	dialog.run()


cmds = (("hello", hello),)

