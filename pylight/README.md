# Pylight

This tool searches for:
- `~/.local/share/recently-used.xbel`
	- opened using `xdg-open`
- Files and Directories if query beginns with `/` or in `$HOME`
	- opened using `xdg-open`
- Executables in `$PATH` and `pylight/scripts`
	- `Enter` runs `executable args... &> /dev/null & disown`
	- `Enter+AltGr` opens a new gnome-terminal to run the command in

Type `help` to open this document, `q` or `quit` to quit doing nothing.

In Gnome, you can create a global shortcut to run the `pylight.py` script like this:
Settings > Devices > Keyboard > Shortcuts



TODO:
- setup-script to create a shortcut (`Super+Spacebar`)
- quit by hitting `ESC`
- handle urls (`http://`, `ftp://`, `https://`, `ssh://`)
- Label showing `executable`, `filepath`, `pylight-command`
- When autocompleting, replace recently-used by full path and add $HOME in front of filename if path rel. to $HOME
- fix bugs
- add files opened by pylight to `~/.local/share/recently-used.xbel`
- when no input: autocomplete with most/recently used cmds (`telegram`, `dzrplayer`, ... for me)
- single instance

Bugs:
- Whitespace in Paths makes everything go crazy
- No autocompletion when adding `/` behind a autocompleted path

