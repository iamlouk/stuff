package main

import (
    "fmt"
)

type Formula interface {
    Eval(atoms []uint8) bool
    String() string
}



type Atom struct {
    name string
    id int
}

func (a Atom) Eval(atoms []uint8) bool { return atoms[a.id] > 0 }
func (a Atom) String() string { return a.name }



type True struct {}

func (t True) Eval(atoms []uint8) bool { return true }
func (t True) String() string { return "⊤" }



type False struct {}

func (f False) Eval(atoms []uint8) bool { return false }
func (f False) String() string { return "⊥" }



type Negation struct { arg Formula }

func (neg Negation) Eval(atoms []uint8) bool { return !neg.arg.Eval(atoms) }
func (neg Negation) String() string { return fmt.Sprintf("¬%s", neg.arg.String()) }



type Conjunction struct {
    lhs, rhs Formula
}

func (con Conjunction) Eval(atoms []uint8) bool {
    return con.lhs.Eval(atoms) && con.rhs.Eval(atoms)
}

func (con Conjunction) String() string {
    return fmt.Sprintf("(%s ∧ %s)", con.lhs.String(), con.rhs.String())
}



type Disjunction struct {
    lhs, rhs Formula
}

func (dis Disjunction) Eval(atoms []uint8) bool {
    return dis.lhs.Eval(atoms) || dis.rhs.Eval(atoms)
}

func (dis Disjunction) String() string {
    return fmt.Sprintf("(%s ∨ %s)", dis.lhs.String(), dis.rhs.String())
}



type Implication struct {
    lhs, rhs Formula
}

func (impl Implication) Eval(atoms []uint8) bool {
    return !impl.lhs.Eval(atoms) || impl.rhs.Eval(atoms)
}

func (impl Implication) String() string {
    return fmt.Sprintf("(%s → %s)", impl.lhs.String(), impl.rhs.String())
}



type Equivalence struct {
    lhs, rhs Formula
}

func (eq Equivalence) Eval(atoms []uint8) bool {
    return eq.lhs.Eval(atoms) == eq.rhs.Eval(atoms)
}

func (eq Equivalence) String() string {
    return fmt.Sprintf("(%s ↔ %s)", eq.lhs.String(), eq.rhs.String())
}



type Contra struct {
    lhs, rhs Formula
}

func (con Contra) Eval(atoms []uint8) bool {
    a, b := con.lhs.Eval(atoms), con.rhs.Eval(atoms)
    return (a && !b) || (!a && b)
}

func (con Contra) String() string {
    return fmt.Sprintf("(%s ⊕ %s)", con.lhs.String(), con.rhs.String())
}

