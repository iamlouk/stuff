package main

import (
    "fmt"
    "unicode"
    "bufio"
    "os"
    "io"
    "strings"
    "strconv"
)

type Error struct {
    message string
}

func (err Error) Error() string {
    return err.message
}

var savedformulas map[string][]Token

func parse(tokens []Token) (Formula, map[string]int, int, error) {
    atoms := make(map[string]int)
    idcounter, i := 0, 0

    var parse_lvl0 func() (Formula, error)
    var parse_lvl1 func() (Formula, error)
    var parse_lvl2 func() (Formula, error)
    var parse_lvl3 func() (Formula, error)

    parse_lvl0 = func() (Formula, error) {
        return parse_lvl1()
    }

    parse_lvl1 = func() (Formula, error) {
        lhs, err := parse_lvl2()
        if err != nil { return nil, err }
        for i < len(tokens) && (tokens[i].ttype == IMPL || tokens[i].ttype == EQUIV) {
            var rhs Formula
            if tokens[i].ttype == IMPL {
                i++
                rhs, err = parse_lvl2()
                e := new(Implication)
                e.lhs = lhs
                e.rhs = rhs
                lhs = e
            } else if tokens[i].ttype == EQUIV {
                i++
                rhs, err = parse_lvl2()
                e := new(Equivalence)
                e.lhs = lhs
                e.rhs = rhs
                lhs = e
            }
            if err != nil { return nil, err }
        }
        return lhs, nil
    }

    parse_lvl2 = func() (Formula, error) {
        lhs, err := parse_lvl3()
        if err != nil { return nil, err }
        for i < len(tokens) && (tokens[i].ttype == AND || tokens[i].ttype == OR || tokens[i].ttype == XOR) {
            var rhs Formula
            if tokens[i].ttype == AND {
                i++
                rhs, err = parse_lvl3()
                e := new(Conjunction)
                e.lhs = lhs
                e.rhs = rhs
                lhs = e
            } else if tokens[i].ttype == OR {
                i++
                rhs, err = parse_lvl3()
                e := new(Disjunction)
                e.lhs = lhs
                e.rhs = rhs
                lhs = e
            } else if tokens[i].ttype == XOR {
                i++
                rhs, err = parse_lvl3()
                e := new(Contra)
                e.lhs = lhs
                e.rhs = rhs
                lhs = e
            }
            if err != nil { return nil, err }
        }
        return lhs, nil
    }

    parse_lvl3 = func() (Formula, error) {
        if i >= len(tokens) {
            return nil, Error{ message: "unexpected end of formular" }
        }
        token := tokens[i]
        i += 1
        if token.ttype == NEG {
            e := new(Negation)
            arg, err := parse_lvl3()
            if err != nil { return nil, err }
            e.arg = arg
            return e, nil
        }
        if token.ttype == LBRACKET {
            e, err := parse_lvl0()
            if err != nil { return nil, err }
            if i >= len(tokens) || tokens[i].ttype != RBRACKET {
                return nil, Error{ message: "expected closing bracket" }
            }
            i++
            return e, nil
        }
        if token.ttype == TRUE {
            return new(True), nil
        }
        if token.ttype == FALSE {
            return new(False), nil
        }
        if token.ttype == ATOM {
            if id, ok := atoms[token.value]; ok {
                e := new(Atom)
                e.name = token.value
                e.id = id
                return e, nil
            } else {
                atoms[token.value] = idcounter
                e := new(Atom)
                e.name = token.value
                e.id = idcounter
                idcounter++
                return e, nil
            }
        }
        return nil, Error{ message: "unexpected token" }
    }

    expr, err := parse_lvl0()
    if err != nil { return nil, nil, 0, err }
    if i != len(tokens) {
        return nil, nil, 0, Error{ message: "unexpected end of formular" }
    }
    return expr, atoms, idcounter, nil
}

func nextConfig(config []uint8, n int) ([]uint8, bool) {
    if config == nil {
        config = make([]uint8, n)
        for i := 0; i < n; i++ {
            config[i] = 0
        }
        return config, n == 0
    }

    for i := n - 1; i >= 0; i-- {
        if config[i] == 0 {
            config[i] = 1
            for j := i + 1; j < n; j++ {
                config[j] = 0
            }
            return config, false
        }
    }

    return nil, true
}

func padd(str string, l int) string {
    for len(str) < l {
        str = string(' ') + str
    }
    return str
}

func main() {
    reader := bufio.NewReader(os.Stdin)
    savedformulas = make(map[string][]Token)

    fmt.Println("Syntax:")
    fmt.Println("\tformula x, y ::= 0 | 1 | <letters> | x '\\/' y | x '/\\' y | x '->' y | x '<->' y | '~'x | '('x')' | $<name>")
    fmt.Println("\t$<name> ':=' formula")

    for {
        fmt.Print("> ")
        text, err := reader.ReadString('\n')
        if err != nil {
            if err != io.EOF {
                panic(err)
            }
            break
        }
        if index := strings.Index(text, "#"); index >= 0 {
            text = text[:index]
        }

        name := ""
        if strings.HasPrefix(text, "$") {
            runes, i := []rune(text), 1
            for ; i < len(runes) && (unicode.IsLetter(runes[i]) || unicode.IsDigit(runes[i])); i++ {
                name += string(runes[i])
            }
            for ; i < len(runes) && unicode.IsSpace(runes[i]); i++ {

            }
            if len(name) > 0 && strings.Index(text, ":=") == i {
                i += 2
                text = string(runes[i:])
            } else {
                name = ""
            }
        }

        tokens, err := tokenize(text)
        if err != nil {
            fmt.Fprintf(os.Stderr, "Tokenizer: %s\n", err.Error())
            continue
        }
        if len(tokens) == 0 { continue }

        expr, atoms, n, err := parse(tokens)
        if err != nil {
            fmt.Fprintf(os.Stderr, "Parser: %s\n", err.Error())
            continue
        }

        if len(name) > 0 {
            savedformulas[name] = tokens
        }

        keys, maxlen := make([]string, n), 0
        for k, v := range atoms {
            keys[v] = k
            if len(k) > maxlen {
                maxlen = len(k)
            }
        }

        fmt.Printf("Input: %s\n[ ", expr.String())
        for i := 0; i < n; i++ {
            if i + 1 == n {
                fmt.Printf("%v ", padd(keys[i], maxlen))
            } else {
                fmt.Printf("%v, ", padd(keys[i], maxlen))
            }
        }
        fmt.Printf("]\n")

        for config, done := nextConfig(nil, n); !done; config, done = nextConfig(config, n) {
            fmt.Printf("[ ")
            for i := 0; i < n; i++ {
                if i + 1 == n {
                    fmt.Printf("%v ", padd(strconv.Itoa(int(config[i])), maxlen))
                } else {
                    fmt.Printf("%v, ", padd(strconv.Itoa(int(config[i])), maxlen))
                }
            }

            if expr.Eval(config) {
                fmt.Printf("] » 1\n")
            } else {
                fmt.Printf("] » 0\n")
            }
        }
    }
}
