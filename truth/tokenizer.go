package main

import (
    "unicode"
)

const (
    NEG int = iota
    AND
    OR
    IMPL
    EQUIV
    XOR

    TRUE
    FALSE

    ATOM
    LBRACKET
    RBRACKET
)

type Token struct {
    ttype int
    value string
}

func tokenize(str string) ([]Token, error) {
    tokens := make([]Token, 0)
    runes := []rune(str)

    reserved := map[string]int{
        "not": NEG,
        "and": AND,
        "or": OR,
        "xor": XOR,
    }

    for i := 0; i < len(runes); i++ {
        switch c := runes[i]; c {
        case ' ', '\t', '\n':
            break
        case '1':
            tokens = append(tokens, Token{ ttype: TRUE })
        case '0':
            tokens = append(tokens, Token{ ttype: FALSE })
        case '!', '~':
            tokens = append(tokens, Token{ ttype: NEG })
        case '&':
            tokens = append(tokens, Token{ ttype: AND })
        case '|':
            tokens = append(tokens, Token{ ttype: OR })
        case '^':
            tokens = append(tokens, Token{ ttype: XOR })
        case '<':
            if i + 2 < len(runes) && runes[i + 1] == '-' && runes[i + 2] == '>' {
                tokens = append(tokens, Token{ ttype: EQUIV })
                i += 2
            } else {
                return nil, Error{ message: "syntax error" }
            }
        case '-':
            if i + 1 < len(runes) && runes[i + 1] == '>' {
                tokens = append(tokens, Token{ ttype: IMPL })
                i += 1
            } else {
                tokens = append(tokens, Token{ ttype: NEG })
            }
        case '/':
            if i + 1 < len(runes) && runes[i + 1] == '\\' {
                tokens = append(tokens, Token{ ttype: AND })
                i += 1
            } else {
                return nil, Error{ message: "syntax error" }
            }
        case '\\':
            if i + 1 < len(runes) && runes[i + 1] == '/' {
                tokens = append(tokens, Token{ ttype: OR })
                i += 1
            } else {
                return nil, Error{ message: "syntax error" }
            }
        case '(', '[':
            tokens = append(tokens, Token{ ttype: LBRACKET })
        case ')', ']':
            tokens = append(tokens, Token{ ttype: RBRACKET })
        case '$':
            i++
            name := ""
            for ; i < len(runes) && (unicode.IsLetter(runes[i]) || unicode.IsDigit(runes[i])); i++ {
                name += string(runes[i])
            }
            i--
            if saved, exists := savedformulas[name]; exists {
                tokens = append(tokens, Token{ ttype: LBRACKET })
                tokens = append(tokens, saved...)
                tokens = append(tokens, Token{ ttype: RBRACKET })
            } else {
                return nil, Error{ message: "$<formula> not found" }
            }
        default:
            if unicode.IsLetter(c) || unicode.IsDigit(c) {
                str := string(c)
                i++
                for ; i < len(runes) && (unicode.IsLetter(runes[i]) || unicode.IsDigit(runes[i])); i++ {
                    str += string(runes[i])
                }
                if ttype, ok := reserved[str]; ok {
                    tokens = append(tokens, Token{ ttype: ttype })
                } else {
                    tokens = append(tokens, Token{ ttype: ATOM, value: str })
                }
                i--
                break
            }
            return nil, Error{ message: "syntax error" }
        }
    }

    return tokens, nil
}
